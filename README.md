# CRUD - Library Catalogs

## Overview

This project is a web application for managing catalogs related to a library's resources, focusing specifically on catalogs of countries and genres. 

It implements CRUD (Create, Read, Update, Delete) operations for these catalogs. 

The application is developed using ASP.NET Core MVC (Model-View-Controller) framework with .NET 7 and utilizes a SQL Server database hosted locally. The database is named "Library" and before running the application, it is necessary to execute the `CreateDb.sql` and later `LibraryDb.sql` script located in the "Scripts" folder within the project to create the required database schema.

## Prerequisites

Before running this application, ensure you have the following installed:

- [.NET 7 SDK](https://dotnet.microsoft.com/download/dotnet/7.0)
- [Microsoft Visual Studio Community 2022 Version 17.4.3](https://visualstudio.microsoft.com/vs/community/)
- [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms)
- SQL Server instance with a database named "Library"

## Getting Started

1. Clone this repository to your local machine.
2. Open the solution file (`CRUD-Library-Catalogs.sln`) in Microsoft Visual Studio Community 2022 Version 17.4.3.
3. Ensure you have a SQL Server instance running, and execute the `LibraryDb.sql` script located in the "Scripts" folder within the project to create the "Library" database and necessary schema.
4. Modify the connection string in the `appsettings.json` file located in the project root directory to point to your local SQL Server instance.
5. Run the application by pressing F5 or clicking on the "Start" button in Visual Studio.
6. Once the application is running, navigate to localhost in your web browser.

## Project Structure

The project structure is as follows:

- **Controllers**: Contains controller classes for handling user requests and rendering views.
- **Models**: Defines the data model classes used for the countries and genres catalogs.
- **Views**: Contains Razor views that display the user interface.
- **Script**: Contains SQL scripts (`CreateDb.sql`, `LibraryDb.sql`) to create the database and schema.

## Contribution

Contributions to this project are welcome! If you encounter any issues or have ideas for improvements, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
