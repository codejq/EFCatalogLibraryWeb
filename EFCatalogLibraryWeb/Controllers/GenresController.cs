﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFCatalogLibraryWeb.Models;

namespace EFCatalogLibraryWeb.Controllers
{
    public class GenresController : Controller
    {
        private readonly LibraryContext _context;

        public GenresController(LibraryContext context)
        {
            _context = context;
        }

        // GET: Genres
        public async Task<IActionResult> Index()
        {
              return _context.TblGenres != null ? 
                          View(await _context.TblGenres.ToListAsync()) :
                          Problem("Entity set 'LibraryContext.TblGenres'  is null.");
        }

        // GET: Genres/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TblGenres == null)
            {
                return NotFound();
            }

            var tblGenre = await _context.TblGenres
                .FirstOrDefaultAsync(m => m.IdGenre == id);
            if (tblGenre == null)
            {
                return NotFound();
            }

            return View(tblGenre);
        }

        // GET: Genres/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Genres/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdGenre,Name")] TblGenre tblGenre)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblGenre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblGenre);
        }

        // GET: Genres/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TblGenres == null)
            {
                return NotFound();
            }

            var tblGenre = await _context.TblGenres.FindAsync(id);
            if (tblGenre == null)
            {
                return NotFound();
            }
            return View(tblGenre);
        }

        // POST: Genres/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdGenre,Name")] TblGenre tblGenre)
        {
            if (id != tblGenre.IdGenre)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblGenre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblGenreExists(tblGenre.IdGenre))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblGenre);
        }

        // GET: Genres/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TblGenres == null)
            {
                return NotFound();
            }

            var tblGenre = await _context.TblGenres
                .FirstOrDefaultAsync(m => m.IdGenre == id);
            if (tblGenre == null)
            {
                return NotFound();
            }

            return View(tblGenre);
        }

        // POST: Genres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TblGenres == null)
            {
                return Problem("Entity set 'LibraryContext.TblGenres'  is null.");
            }
            var tblGenre = await _context.TblGenres.FindAsync(id);
            if (tblGenre != null)
            {
                _context.TblGenres.Remove(tblGenre);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblGenreExists(int id)
        {
          return (_context.TblGenres?.Any(e => e.IdGenre == id)).GetValueOrDefault();
        }
    }
}
