﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFCatalogLibraryWeb.Models;

namespace EFCatalogLibraryWeb.Controllers
{
    public class CountriesController : Controller
    {
        private readonly LibraryContext _context;

        public CountriesController(LibraryContext context)
        {
            _context = context;
        }

        // GET: Countries
        public async Task<IActionResult> Index()
        {
              return _context.TblCountries != null ? 
                          View(await _context.TblCountries.ToListAsync()) :
                          Problem("Entity set 'LibraryContext.TblCountries'  is null.");
        }

        // GET: Countries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TblCountries == null)
            {
                return NotFound();
            }

            var tblCountry = await _context.TblCountries
                .FirstOrDefaultAsync(m => m.IdCountry == id);
            if (tblCountry == null)
            {
                return NotFound();
            }

            return View(tblCountry);
        }

        // GET: Countries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCountry,Name,AreaCode,Iso31661Alpha2")] TblCountry tblCountry)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblCountry);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblCountry);
        }

        // GET: Countries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TblCountries == null)
            {
                return NotFound();
            }

            var tblCountry = await _context.TblCountries.FindAsync(id);
            if (tblCountry == null)
            {
                return NotFound();
            }
            return View(tblCountry);
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCountry,Name,AreaCode,Iso31661Alpha2")] TblCountry tblCountry)
        {
            if (id != tblCountry.IdCountry)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblCountry);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblCountryExists(tblCountry.IdCountry))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblCountry);
        }

        // GET: Countries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TblCountries == null)
            {
                return NotFound();
            }

            var tblCountry = await _context.TblCountries
                .FirstOrDefaultAsync(m => m.IdCountry == id);
            if (tblCountry == null)
            {
                return NotFound();
            }

            return View(tblCountry);
        }

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TblCountries == null)
            {
                return Problem("Entity set 'LibraryContext.TblCountries'  is null.");
            }
            var tblCountry = await _context.TblCountries.FindAsync(id);
            if (tblCountry != null)
            {
                _context.TblCountries.Remove(tblCountry);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblCountryExists(int id)
        {
          return (_context.TblCountries?.Any(e => e.IdCountry == id)).GetValueOrDefault();
        }
    }
}
