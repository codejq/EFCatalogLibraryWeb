﻿using System;
using System.Collections.Generic;

namespace EFCatalogLibraryWeb.Models;

public partial class TblCountry
{
    public int IdCountry { get; set; }

    public string Name { get; set; } = null!;

    public string AreaCode { get; set; } = null!;

    public string Iso31661Alpha2 { get; set; } = null!;
}
