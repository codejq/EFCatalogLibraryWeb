﻿using System;
using System.Collections.Generic;

namespace EFCatalogLibraryWeb.Models;

public partial class TblGenre
{
    public int IdGenre { get; set; }

    public string Name { get; set; } = null!;
}
