﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EFCatalogLibraryWeb.Models;

public partial class LibraryContext : DbContext
{
    public LibraryContext()
    {
    }

    public LibraryContext(DbContextOptions<LibraryContext> options)
        : base(options)
    {
    }

    public virtual DbSet<TblCountry> TblCountries { get; set; }

    public virtual DbSet<TblGenre> TblGenres { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("server=localhost; database=Library; user id=<<USERNAME>>; password=<<PASSWORD>>; TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TblCountry>(entity =>
        {
            entity.HasKey(e => e.IdCountry).HasName("PK__TBL_COUN__294318F4A11E4A36");

            entity.ToTable("TBL_COUNTRIES");

            entity.Property(e => e.IdCountry).HasColumnName("id_country");
            entity.Property(e => e.AreaCode)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("area_code");
            entity.Property(e => e.Iso31661Alpha2)
                .HasMaxLength(2)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("iso_3166_1_alpha_2");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("name");
        });

        modelBuilder.Entity<TblGenre>(entity =>
        {
            entity.HasKey(e => e.IdGenre).HasName("PK__TBL_GENR__CB767C6906C96208");

            entity.ToTable("TBL_GENRES");

            entity.Property(e => e.IdGenre).HasColumnName("id_genre");
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("name");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
